// Express Setup

const express = require("express")

// mongoose is a package that allows creation of schemas to model our data structures
const mongoose = require("mongoose");

const app = express()
const port = 3001;

// [Section] - Mongoose Connection
// Mongoose uses the 'connect' function to connect to the cluster in our MongoDb Atlas

/*
	It takes 2 argumerts:
	1. Connection string from our MongoDB Atlas.
	2. Object that contains the middlewares/standards that MongoDb uses.
*/

mongoose.connect(`mongodb://jleizl93:admin123@ac-floig6u-shard-00-00.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-01.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-02.2zb8dt2.mongodb.net:27017/S35?ssl=true&replicaSet=atlas-12lysg-shard-0&authSource=admin&retryWrites=true&w=majority`, {
    // {newUrlParser: true} - it allows us to avoid any current and/or future error while connecting to MongoDB
    useNewUrlParser: true,
    // useUnifiedTopology if "false" by default. Set to true to opt in to using the MongoDb driver's new connection management engine
    useUnifiedTopology: true
})

// Initializes the mongoose connection to the MongoDb Db by assigning 'mongoose.connection' to the 'db' variable.
let db = mongoose.connection

// Lister to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console based of the event (error or successful)
db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'))


// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// CREATING A SCHEMA
const taskSchema = new mongoose.Schema({
    // Define the fields with their corresponding data types
    // For a task, it needs a "task name" and its data type will be "String"
    name: String,
    // There is a field called "status" that has a dta type of "String" and the default value is "pending"
    status: {
        type: String,
        default: 'Pending'
    }
})

// MODELS
// The variable/object "Task" can now be used to run commands for interating with our DB.
// "Task" is capitalized following the MVC approach for the naming conventions
// Models must be in singular form and capitalized
// The first parameter is used to specify the Name of the collection where we will store our data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in our MongoDB collection/s
const Task = mongoose.model('Task', taskSchema);


// Creating the routes
// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Create a user route
app.post('/tasks', (req, res) => {
    // business logic
    Task.findOne({ name: req.body.name }, (error, result) => {
        if (error) {
            return res.send(error)
        } else if (result != null && result.name == req.body.name) {
            return res.send('Duplicate task found')
        } else {
            let newTask = new Task({
                name: req.body.name
            })
            newTask.save((error, savedTask) => {
                if (error) {
                    return console.error(error)
                } else {
                    return res.status(201).send('New Task Created!')
                }
            })
        }
    })
})

// Get all users from collection - get all users route

app.get('/tasks', (req, res) => {
    Task.find({}, (error, result) => {
        if (error) {
            return res.send(error)
        } else {
            return res.status(200).json({
                tasks: result
            })
        }
    })
})


// ACTIVITY CODE

// 1. 
const userSchema = new mongoose.Schema({
    
    username: String,
   	password: String,
})

// 2. 

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// 3. 

app.post('/signup', (req, res) => {
    
    User.findOne({ username: req.body.username }, (error, result) => {
        if (error) {
            return res.send(error)
        } else if (result != null && result.username == req.body.username) {
            return res.send('User already exist!')
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save((error, savedUser) => {
                if (error) {
                    return console.error(error)
                } else {
                    return res.status(201).send('New user registered')
                }
            })
        }
    })
})

app.get('/signup', (req, res) => {
    User.find({}, (error, result) => {
        if (error) {
            return res.send(error)
        } else {

            return res.status(200).json({
                users: result
            })
        }
    })
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))